# AVENTURO KATO

By Rachel J. Morris (c) 2012

moosader.com

**Year:** 2012

**Tools:** Lua/Love2D

## What is this?
This is several things...:
* My boyfriend Richard's birthday present
* My competition entry for the Moosader.com 8th competition
* Sample code made with LOVE2D/Lua
* An excuse to make public-domain art for art.devsader.com

## About the game
This game will have its maps and enemies created via
random generation.

This is a self-playing game, but you can also override
what your cat does (not currently implemented).  
If you leave it alone, your cat
will explore the map on its own, and fight battles.
If the cat gets knocked out, it will snooze for a bit
then get back to it!

## How to play

Use the Arrow Keys to navigate menus, and Enter to select.

From the game, you can press ESCAPE to view the INVENTORY and STATS screen.

## Links!
* [Moosader Community Randomly/Proceudral Generation Competition](http://www.moosader.com/competitions/compo8-procedural-and-random-generation)
* [Moosader.com](http://www.moosader.com/)
* [ArtSader](http://art.devsader.com/)

## Screenshots

![Screenshot of game](screenshot-kato.png)
