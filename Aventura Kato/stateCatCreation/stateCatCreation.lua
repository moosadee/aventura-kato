-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- Cat Creation

require "player/player"
require "controllers/graphicController"
require "data/colorData"

stateCatCreation = {
	name = "CatCreation"
}

function stateCatCreation:Load()
	self:LoadLoveItems()
	self:LoadVariables()
end

function stateCatCreation:Update( deltaTime )
	retval = self:HandleInput()
	self:Animate( deltaTime )
	return retval 
end

function stateCatCreation:Draw()
	self:DrawMenu()
end

-- **********************************
-- ** Additional functionality
-- **********************************

function stateCatCreation:LoadLoveItems()
	imgArrows = love.graphics.newImage( "content/graphics/menu-arrows.png" )
	imgCatTesla = love.graphics.newImage( "content/graphics/cat-tesla.png" )
	imgCatMagellan = love.graphics.newImage( "content/graphics/cat-magellan.png" )
	imgCatAda = love.graphics.newImage( "content/graphics/cat-ada.png" )
	
	quadLeftArrow = love.graphics.newQuad( 0, 0, 30, 50, 60, 100 )
	quadRightArrow = love.graphics.newQuad( 0, 50, 30, 50, 60, 100 )
	
	quadCat = love.graphics.newQuad( 0, 0, 100, 100, 200, 700 )
	
	fntSmall = love.graphics.newFont( 9 )
	fntHeader = love.graphics.newFont( 14 )
end

function stateCatCreation:LoadVariables()
	stateCatCreation = {
		name = "CatCreation"
	}

	cats = {}
	cats[1] = { name = "Tesla" }
	cats[2] = { name = "Magellan" }
	cats[3] = { name = "Ada" }
	
	currentCatIdx = 1
	blinkCounter = 0
end

function stateCatCreation:HandleInput()
	if ( keyboard:InputOK() ) then
		if ( love.keyboard.isDown( "left" ) ) then
			currentCatIdx = currentCatIdx - 1
			if ( currentCatIdx < 1 ) then 
				currentCatIdx = 3 
			end
			keyboard:FreezeInput()
		elseif ( love.keyboard.isDown( "right" ) ) then
			currentCatIdx = currentCatIdx + 1
			if ( currentCatIdx > 3 ) then 
				currentCatIdx = 1 
			end
			keyboard:FreezeInput()
		end
		
		if ( love.keyboard.isDown( "return" ) ) then
			self:SetupPlayer()
			return "stateGame"
		end
	end
	
	return ""
end

function stateCatCreation:SetupPlayer()
	-- Set cat appearance (graphic & color)
	player.catType = cats[currentCatIdx].name

	if ( player.catType == "Tesla" ) then
		player.image = imgCatTesla
	elseif ( player.catType == "Magellan" ) then
		player.image = imgCatMagellan
	else
		player.image = imgCatAda
	end
	
	player.color = colors:GetRandomColor()
end

function stateCatCreation:Animate( deltaTime )
	blinkCounter = blinkCounter + 0.005 - deltaTime
	if ( blinkCounter < 1 ) then 
		quadLeftArrow:setViewport( 30, 0, 30, 50 )
		quadRightArrow:setViewport( 30, 50, 30, 50 )
		quadCat:setViewport( 100, 0, 100, 100 )
	elseif ( blinkCounter < 2 ) then
		quadLeftArrow:setViewport( 0, 0, 30, 50 )
		quadRightArrow:setViewport( 0, 50, 30, 50 )
		quadCat:setViewport( 0, 0, 100, 100 )
	else
		blinkCounter = 0
	end
end

function stateCatCreation:DrawMenu()
	-- Background
	gfx:DrawRectangle( "fill", { x = 0, y = 0 }, { w = 200, h = 200 }, colorKey.white )
	
	-- Menu Arrows
	scrollerY = 100
	love.graphics.setColor( 255, 255, 255, 255 )
	gfx:DrawQuad( imgArrows, quadLeftArrow, { x = 0, y = scrollerY }, colorKey.white )
	gfx:DrawQuad( imgArrows, quadRightArrow, { x = 200-30, y = scrollerY }, colorKey.white )
	
	if ( cats[currentCatIdx].name == "Tesla" ) then
		catImage = imgCatTesla
	elseif ( cats[currentCatIdx].name == "Magellan" ) then
		catImage = imgCatMagellan
	else
		catImage = imgCatAda
	end
	
	gfx:DrawQuad( catImage, quadCat, 
		{ x = 100-50, y = scrollerY-40 }, colorKey.white )
	
	gfx:DrawText( "Create-A-Cat", 
		{ x = 50, y = 5 }, fntHeader, colorKey.black )
	gfx:DrawText( "Press [LEFT] and [RIGHT] to", 
		{ x = 5, y = 30 }, fntSmall, colorKey.black )
	gfx:DrawText( "scroll through cats.", 
		{ x = 5, y = 40 }, fntSmall, colorKey.black )
	gfx:DrawText( "Press [ENTER] to choose cat", 
		{ x = 5, y = 60 }, fntSmall, colorKey.black )
	gfx:DrawText( "and start game.", 
		{ x = 5, y = 70 }, fntSmall, colorKey.black )
	gfx:DrawText( cats[currentCatIdx].name, 
		{ x = 80, y = 180 }, fntHeader, colorKey.black )
end