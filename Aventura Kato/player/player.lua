-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

require "controllers/graphicController"
require "data/colorData"

actions = {
	sleeping = "0",
	walkWest = "1",
	walkEast = "2",
	walkSouth = "3",
	walkNorth = "4",
	attacking = "5",
	hurt = "6"
}

-- The Player table.  Things that are "nil" will be filled in
-- during the Cat Creation state.
player = {
	catType = "",
	color = {},
	image = nil,
	frameQuad = nil,
	frame = 0,
	action = 0,
	width = 100,
	height = 100,
	stats = {
		HP = 100,
		Atk = 10,
		Def = 10
	},
	inventory = {},
	steps = 0,
	stepsThisMap = 0,
	totalSteps = 0,
	totalKills = 0,
	speed = 0.003,
	walkSpeed = 0.003,
	runSpeed = 0.01,
	path = {}
}

-- This is to test the Game State without going through the menus
function player:TestSetup()
	player.catType = "Magellan"
	player.image = love.graphics.newImage( "content/graphics/cat-magellan.png" )
	player.color = colors:GetRandomColor()
end

function player:Setup()
	self.action = actions.walkWest
	self.frameQuad = love.graphics.newQuad( 
		0, 0, self.width, self.height, 
		self.image:getWidth(), self.image:getHeight() )
end

function player:Update()
    if ( self.action ~= actions.sleeping ) then
        self:DecideWhereToGo()
        
        if ( love.keyboard.isDown( "c" ) ) then player.color = colors:GetRandomColor() end
        
        self.stats.HP = self.stats.HP - self.speed * 2
        if ( self.stats.HP < 10 ) then
            self.action = actions.sleeping -- Sleep
        end
    else
        -- Sleeping
        self.stats.HP = self.stats.HP + self.speed * 5
        if ( self.stats.HP >= 100 ) then
            self.action = actions.walkSouth
        end
    end

    self.frame = self.frame + self.speed
    if ( self.frame >= 2 ) then
        self.frame = 0
    end
    
    self.frameQuad:setViewport( 
        math.floor(self.frame) * self.width,
        self.action * self.height,
        self.width, self.height )
end

function player:DecideWhereToGo()
	table.insert( self.path, self.action ) -- Path recording
	-- TODO: Clean this up
	self.steps = self.steps + self.speed
	self.totalSteps = self.totalSteps + self.speed
	self.stepsThisMap = self.stepsThisMap + self.speed
	
	if ( self.steps > 20 ) then
		-- Maybe change directions
		changeDirection = math.random( 1, 4 )
		if ( changeDirection < 5 ) then
			self.action = changeDirection
			self.steps = 0
		end
	end
end

function player:GetRank()
	if ( player.totalSteps < 25 ) then
		return "Pudgey Cat"
	elseif ( player.totalSteps < 50 ) then
		return "Curious Cat"
	elseif ( player.totalSteps < 75 ) then
		return "Wanderer Cat"
	elseif ( player.totalSteps < 100 ) then
		return "Ronin Neko"
	elseif ( player.totalSteps < 200 ) then
		return "Unsanitary Kitty"
	elseif ( player.totalSteps < 300 ) then
		return "Really Sweaty Cat"
	elseif ( player.totalSteps < 400 ) then
		return "Adventurous Cat"
	elseif ( player.totalSteps < 500 ) then
		return "Hero Cat"
	elseif ( player.totalSteps < 1000 ) then
		return "Titan Cat"
	elseif ( player.totalSteps < 2500 ) then
		return "Flea-infested Cat"
	elseif ( player.totalSteps < 5000 ) then
		return "One Mile Milestone Cat"
	elseif ( player.totalSteps < 7500 ) then
		return "Space Cat"
	elseif ( player.totalSteps < 10000 ) then
		return "Godly Catly"
	end
end

function player:Draw()
	gfx:DrawQuad( self.image, self.frameQuad, { x = 50, y = 70 }, self.color )
end
