-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

items = {
	rarity1 = {	
		total = 4,
		item1 = {
			name = "Fajro Gem",
			count = 0
		},
		item2 = {
			name = "Akvo Gem",
			count = 0
		},
		item3 = {
			name = "Aero Gem",
			count = 0
		},
		item4 = {
			name = "Tero Gem",
			count = 0
		},
	},
	rarity2 = {
		total = 6,
		item1 = {
			name = "Bovino Plushie",
			count = 0
		},
		item2 = {
			name = "Elefanto Plushie",
			count = 0
		},
		item3 = {
			name = "Rano Plushie",
			count = 0
		},
		item4 = {
			name = "Vulpo Plushie",
			count = 0
		},
		item5 = {
			name = "Kuniklo Plushie",
			count = 0
		},
		item6 = {
			name = "Urso Plushie",
			count = 0
		}
	},
	rarity3 = {
		total = 9,
		item1 = {
			name = "Rugxa Candy",
			count = 0
		},
		item2 = {
			name = "Blua Candy",
			count = 0
		},
		item3 = {
			name = "Verda Candy",
			count = 0
		},
		item4 = {
			name = "Flava Candy",
			count = 0
		},
		item5 = {
			name = "Purpura Candy",
			count = 0
		},
		item6 = {
			name = "Blanka Candy",
			count = 0
		},
		item7 = {
			name = "Nigra Candy",
			count = 0
		},
		item8 = {
			name = "Bruna Candy",
			count = 0
		},
		item9 = {
			name = "Rozkolora Candy",
			count = 0
		}
	}
}

function items:AddToInventory()-- Get an item!  Is it rarity 1, 2, or 3
	local rarity = math.random( 1, 100 )
	
	if ( rarity < 10 ) then
		itemCategory = items.rarity1
	elseif ( rarity < 40 ) then
		itemCategory = items.rarity2
	else
		itemCategory = items.rarity3
	end
	
	itemIdx = math.random( 1, itemCategory.total )
	
	itemCategory["item" .. itemIdx].count =
		itemCategory["item" .. itemIdx].count + 1
	
	return itemCategory["item" .. itemIdx].name
end

