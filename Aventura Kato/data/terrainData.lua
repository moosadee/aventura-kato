-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

require "controllers/soundController"
require "controllers/graphicController"
require "data/colorData"

terrains = {
	grasslands = {
		sky = { r = 100, g = 255, b = 200 },
		ground = { r = 100, g = 255, b = 100 },
		name = "Grasslands"
	},
	desert = {
		sky = { r = 0, g = 130, b = 200 },
		ground = { r = 140, g = 100, b = 50 },
		name = "Desert"
	},
	mountain = {
		sky = { r = 125, g = 60, b = 170 },
		ground = { r = 170, g = 225, b = 230 },
		name = "Mountain"
	},
	cave = {
		sky = { r = 85, g = 85, b = 85 },
		ground = { r = 170, g = 170, b = 170 },
		name = "Cave"
	},
	mars = {
		sky = { r = 255, g = 150, b = 0 },
		ground = { r = 160, g = 65, b = 0 },
		name = "Mars"
	}
}

function terrains:RandomTerrain()
	-- Change terrain
	local newTerrain = math.random( 1, 5 )
	if ( newTerrain == 1 ) then
		newTerrain = terrains.grasslands
	elseif ( newTerrain == 2 ) then
		newTerrain = terrains.desert
	elseif ( newTerrain == 3 ) then
		newTerrain = terrains.mountain
	elseif ( newTerrain == 4 ) then
		newTerrain = terrains.cave
	else
		newTerrain = terrains.mars
	end
	
	--music:PlayMusic( newTerrain.name )
	return newTerrain
end

function DrawScene( terrain )
	gfx:DrawRectangle( "fill", { x = 0, y = 0 }, { w = 200, h = 150 }, terrain.sky )
	gfx:DrawRectangle( "fill", { x = 0, y = 150 }, { w = 200, h = 50 }, terrain.ground )
end