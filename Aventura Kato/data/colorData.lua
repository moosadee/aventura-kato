-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

colors = {
	{ r = 255, 	g = 255, 	b = 255 }, 	-- White
	{ r = 255, 	g = 0, 		b = 0 },	-- Red
	{ r = 0, 	g = 255, 	b = 0 },	-- Green
	{ r = 0, 	g = 0, 		b = 255 },	-- Blue
	{ r = 255, 	g = 150, 	b = 0 },	-- Orange
	{ r = 255, 	g = 255, 	b = 0 },	-- Yellow
	{ r = 0, 	g = 150, 	b = 255 },	-- Light blue
	{ r = 130, 	g = 72, 	b = 174 },	-- Purple
	{ r = 175, 	g = 175, 	b = 175 },	-- Light grey
	{ r = 125, 	g = 50, 	b = 0 },	-- Brown
	{ r = 250, 	g = 100, 	b = 150 },	-- Pink
	{ r = 100,	g = 100, 	b = 100 },	-- Dark Grey
	{ r = 0, 	g = 0, 		b = 0 }		-- Black
}

colorKey = {
	white 		= colors[1],
	red 		= colors[2],
	green 		= colors[3],
	blue 		= colors[4],
	orange 		= colors[5],
	yellow 		= colors[6],
	lightBlue 	= colors[7],
	purple 		= colors[8],
	lightGrey 	= colors[9],
	brown 		= colors[10],
	pink 		= colors[11],
	darkGrey 	= colors[12],
	black 		= colors[13]
}

function colors:GetRandomColor()
	idx = math.random( 1, 11 )
	return colors[idx]
end
