-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- Title State

require "controllers/soundController"
require "controllers/graphicController"
require "data/colorData"

stateTitle = {
	name = "Title"
}

function stateTitle:Load()
	self:LoadLoveItems()
	self:LoadVariables()
end

function stateTitle:Update( deltaTime )
	retval = self:HandleInput()
	self:Animate( deltaTime )
	return retval -- Helps the controller know to change state
end

function stateTitle:Draw()
	self:DrawTitle()
end

-- **********************************
-- ** Additional functionality
-- **********************************

function stateTitle:LoadLoveItems()
	fntHeader = love.graphics.newFont( 12 )
	imgTitle = love.graphics.newImage( "content/graphics/screen-title.png" )
	music:PlayMusic( "Title" )
end

function stateTitle:LoadVariables()
	blinkCounter = 0
end

function stateTitle:Animate( deltaTime )
	blinkCounter = blinkCounter + 0.005 - deltaTime
	if ( blinkCounter >= 2 ) then blinkCounter = 0 end
end

function stateTitle:DrawTitle()
	gfx:DrawImage( imgTitle, { x = 0, y = 0 }, colorKey.white )
	if ( blinkCounter < 1 ) then 
		gfx:DrawText( "Hit [ENTER] to start", 
			{ x = 35, y = 170 }, fntHeader, colorKey.black )
	end
end

function stateTitle:HandleInput()
	if ( love.keyboard.isDown( "return" ) ) then
		return "stateCatCreation"
	end
	return ""
end
