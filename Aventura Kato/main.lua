-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

require "controllers/stateController"
require "controllers/soundController"
require "controllers/keyboardController"

function love.load()
	stateController:Load()
end

function love.update( deltaTime )
	stateController:Update( deltaTime )
	keyboard:Update( deltaTime )
	if ( love.keyboard.isDown( "s" ) ) then music:ToggleMusic() end
end

function love.draw()
	love.graphics.setColor( 0, 0, 50, 255 )
	love.graphics.rectangle( "fill", 0, 0, love.graphics:getWidth(), love.graphics:getHeight() )
	stateController:Draw()
	gfx:DrawRectangle( "line", { x = 0, y = 0 }, { w = 200, h = 200 }, colorKey.blue )
end
