-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- Game State

require "player/player"
require "data/terrainData"
require "data/itemData"
require "controllers/graphicController"
require "data/colorData"

stateGame = {
	name = "Game",
	initialized = false -- Don't want to reset state after it's been loaded
}

itemObtained = {
	name = "",
	counter = 0
}

function stateGame:Load()
	if ( self.initialized == false ) then
		self:Setup()
		player:Setup()
		currentTerrain = terrains:RandomTerrain()
		self.initialized = true
	end
end

function stateGame:Update( deltaTime )
	returnState = self:HandleInput()
	player:Update()
	self:DecideTerrain()
	self:DecideItemFound()
	if ( itemObtained.counter > 0 ) then
		itemObtained.counter = itemObtained.counter - 1
	end
	return returnState
end

function stateGame:Draw()
	self:DrawBackground()
	player:Draw()
	self:DrawHUD()
end

-- **********************************
-- ** Additional functionality
-- **********************************

function stateGame:Setup()
	fntSmall = love.graphics.newFont( 9 )
	fntHeader = love.graphics.newFont( 11 )
end

function stateGame:DrawBackground()
	DrawScene( currentTerrain )
	-- Separates sky from ground
	gfx:DrawLine( { x = 0, y = 150 }, { x = 200, y = 150 }, colorKey.black )
end

function stateGame:DrawHUD()
	gfx:DrawText( "Location", { x = 5, y = 2 }, fntHeader, colorKey.black )
	-- Header
	gfx:DrawText( player.catType .. " Steps", { x = 100, y = 2 }, fntHeader, colorKey.black )
	gfx:DrawText( "Health", { x = 5, y = 30 }, fntHeader, colorKey.black )
	-- Data
	gfx:DrawText( currentTerrain.name, { x = 10, y = 15 }, fntSmall, colorKey.black )
	gfx:DrawText( math.floor(player.totalSteps), { x = 110, y = 15 }, fntSmall, colorKey.black )
    
    -- Health Bar
	local color = nil
    if ( player.stats.HP > 90 ) then 		color = colorKey.green
    elseif ( player.stats.HP > 50 ) then 	color = colorKey.orange
    else 									color = colorKey.red
    end    
	gfx:DrawRectangle( "fill", { x = 45, y = 30 }, { w = math.floor(player.stats.HP), h = 10 }, color )
	gfx:DrawRectangle( "line", { x = 45, y = 30 }, { w = 100, h = 10 }, colorKey.black )
	
	-- Obtained Item
	if ( itemObtained.counter > 0 ) then
		gfx:DrawRectangle( "fill", { x = 0, y = 180 }, { w = 200, h = 20 }, colorKey.white )
		gfx:DrawText( itemObtained.name .. " found!", { x = 50, y = 180 }, fntHeader, colorKey.blue )
	end
end

function stateGame:HandleInput()
	-- Allow player to override cat behavior
	--if ( love.keyboard.isDown( "up" ) ) then
	--	player.actions = actions.walkNorth
	--	player.steps = 0
	--elseif ( love.keyboard.isDown( "down" ) ) then
	--	player.actions = actions.walkSouth
	--	player.steps = 0
	--elseif ( love.keyboard.isDown( "left" ) ) then
	--	player.actions = actions.walkWest
	--	player.steps = 0
	--elseif ( love.keyboard.isDown( "right" ) ) then
	--	player.actions = actions.walkEast
	--	player.steps = 0
	--end
	
	if ( love.keyboard.isDown( "lshift" ) ) then
		player.speed = player.runSpeed
	elseif ( love.keyboard.isDown( "rshift" ) ) then	-- TODO: REMOVE, this is for DEBUG
		player.speed = 1
	else
		player.speed = player.walkSpeed
	end
	
	if ( love.keyboard.isDown( "escape" ) and keyboard:InputOK() ) then
		return "stateMenu"
	end
	return ""
end

function stateGame:DecideTerrain()
	if ( player.stepsThisMap > 30 and math.random( 1, 50 ) == 1 ) then
		currentTerrain = terrains:RandomTerrain()
		player.stepsThisMap = 0
	end
end

function stateGame:DecideItemFound()
	if ( player.steps % 10 == 0 ) then
		found = math.random( 1, 20 )
		if ( found == 1 ) then
			itemObtained.name = items:AddToInventory()
			itemObtained.counter = 100
		end
	end
end


