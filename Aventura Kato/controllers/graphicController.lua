-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

require "data/debug"

gfx = {}

function gfx:DrawText( message, coord, font, color )
	love.graphics.setFont( font )
	love.graphics.setColor( color.r, color.g, color.b, color.a )
	love.graphics.print( message, coord.x + dbg.x, coord.y + dbg.y )
end

function gfx:DrawRectangle( mode, coord, dimen, color )
	love.graphics.setColor( color.r, color.g, color.b, color.a )
	love.graphics.rectangle( mode, coord.x + dbg.x, coord.y + dbg.y, dimen.w, dimen.h )
end

function gfx:DrawLine( coord1, coord2, color )
	love.graphics.setColor( color.r, color.g, color.b )
	love.graphics.line( coord1.x + dbg.x, coord1.y + dbg.y, coord2.x + dbg.x, coord2.y + dbg.y )
end

function gfx:DrawQuad( image, quad, coord, color )
	love.graphics.setColor( color.r, color.g, color.b )
	love.graphics.drawq( image, quad, coord.x + dbg.x, coord.y + dbg.y )
end

function gfx:DrawImage( image, coord, color )
	love.graphics.setColor( color.r, color.g, color.b )
	love.graphics.draw( image, coord.x + dbg.x, coord.y + dbg.y )
end