-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- State Controller
-- Handles keeping track of current state,
-- showing the loading screen, and swaping
-- out states based on return values from
-- the currentState's Update function.

require "stateTitle/stateTitle"
require "stateCatCreation/stateCatCreation"
require "stateGame/stateGame"
require "stateMenu/stateMenu"
require "controllers/graphicController"
require "data/colorData"

currentState = {}

stateController = {}

nowLoading = false

-- Called once at beginning of program
function stateController:Load()
	fntLoading = love.graphics.newFont( 16 )
	fntLoading2 = love.graphics.newFont( 12 )
	-- Load the first state
	self:LoadState( stateTitle )
	
	-- Change to game state for test
	--player:TestSetup()
	--self:LoadState( stateGame )
end

-- Called each cycle
function stateController:Update( deltaTime )
	-- We want to draw before changing states,
	-- so we check this at the beginning of the NEXT update cycle
	if ( nextState ~= "" ) then
		self:SwapStates( nextState )
	end
	
	nextState = currentState:Update( deltaTime )
	if ( nextState ~= "" and keyboard:InputOK() ) then 
		nowLoading = true 
		keyboard:FreezeInput()
	end
end

-- Called each cycle
function stateController:Draw()	
	if ( nowLoading ) then
		self:DrawLoadingScreen()
	else
		currentState:Draw()
	end
end

-- **********************************
-- ** Additional functionality
-- **********************************

function stateController:SwapStates( stateName )
	if ( stateName == "stateCatCreation" ) then
		self:LoadState( stateCatCreation )
	elseif( stateName == "stateGame" ) then
		self:LoadState( stateGame )
	elseif ( stateName == "stateMenu" ) then
		self:LoadState( stateMenu )
	end
	nowLoading = false
end

function stateController:LoadState( state )
	-- Load next state
	currentState = state
	currentState:Load()
end

function stateController:DrawLoadingScreen()
	gfx:DrawRectangle( "fill", { x = 0, y = 0 }, { w = 200, h = 200 }, colorKey.darkGrey )
	gfx:DrawText( "NOW LOADING...", { x = 30, y = 75 }, fntLoading, colorKey.white )
	
	loadMsg = math.random( 1, 4 )
	if 		( loadMsg == 1 ) then
		gfx:DrawText( "So... how 'bout this weather?", 
			{ x = 10, y = 125 }, fntLoading2, colorKey.white )
	elseif 	( loadMsg == 2 ) then
		gfx:DrawText( "1... 2... 3...",
			 { x = 60, y = 125 }, fntLoading2, colorKey.white  )
	elseif 	( loadMsg == 3 ) then
		gfx:DrawText( "Need moar cats",
			 { x = 45, y = 125 }, fntLoading2, colorKey.white  )
	elseif 	( loadMsg == 4 ) then
		gfx:DrawText( "Time to crush rocks",
			 { x = 35, y = 125 }, fntLoading2, colorKey.white  )
		gfx:DrawText( "WITH YOUR FISTS!",
			 { x = 40, y = 140 }, fntLoading2, colorKey.white  )
	else
		gfx:DrawText( "Luh luh luh",
			 { x = 60, y = 125 }, fntLoading2, colorKey.white  )
	end
end
