-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

require "controllers/keyboardController"

musicPath = "content/audio/"

soundEnabled = true
lastPlayedSong = nil

music = {
	Title = "HappyTheme_MikeSchrandt.ogg"
}

function music:PlayMusic( songName )
	-- Stop any audio	
	love.audio.stop()
	lastPlayedSong = songName
	if ( soundEnabled ) then
		songTitle = love.audio.newSource( musicPath .. music[songName], "streaming" )
		love.audio.play( songTitle ) 
	end
end

function music:ToggleMusic()	
	if ( keyboard:InputOK() ) then
		if ( soundEnabled == true ) then
			soundEnabled = false
			love.audio.stop()
		else
			soundEnabled = true
			self:PlayMusic( lastPlayedSong )
		end
		keyboard:FreezeInput()
	end
end